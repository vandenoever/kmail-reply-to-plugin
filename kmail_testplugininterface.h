#pragma once

#include <MessageComposer/PluginEditorInterface>

class MyKMailPluginInterface : public MessageComposer::PluginEditorInterface {
    Q_OBJECT
public:
    explicit MyKMailPluginInterface(QObject *parent = nullptr);
    ~MyKMailPluginInterface() override;

    void createAction(KActionCollection *ac) override;
    void exec() override;
};
