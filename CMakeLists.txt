cmake_minimum_required(VERSION 3.16 FATAL_ERROR)
project(MyKMailPlugin)
set(KF5_MIN_VERSION "5.73.0")
set(KDE_PIM_VERSION "5.15.2")

find_package(ECM ${KF5_MIN_VERSION} CONFIG REQUIRED)
set(CMAKE_MODULE_PATH ${ECM_MODULE_PATH})
include(KDECMakeSettings)
include(KDEInstallDirs)

find_package(KF5I18n ${KF5_MIN_VERSION} CONFIG REQUIRED)
find_package(KF5KIO ${KF5_MIN_VERSION} CONFIG REQUIRED)
find_package(KF5XmlGui ${KF5_MIN_VERSION} CONFIG REQUIRED)
find_package(KF5MessageComposer ${KDEPIM_VERSION} CONFIG REQUIRED)
find_package(KF5PimCommonAkonadi ${KDEPIM_VERSION} CONFIG REQUIRED)
find_package(KF5Libkdepim ${KDEPIM_VERSION} CONFIG REQUIRED)

kcoreaddons_add_plugin(kmail_mykmailplugin
    SOURCES kmail_testplugin.cpp kmail_testplugininterface.cpp
    INSTALL_NAMESPACE kmail/plugineditor)
set_property(TARGET kmail_mykmailplugin PROPERTY AUTOMOC ON)
target_link_libraries(kmail_mykmailplugin
    KF5::MessageComposer KF5::I18n KF5::XmlGui
)
