#include "kmail_testplugininterface.h"
#include <KActionMenu>
#include <KActionCollection>
#include <KLocalizedString>

MyKMailPluginInterface::MyKMailPluginInterface(QObject *parent)
   : MessageComposer::PluginEditorInterface(parent) {
}

MyKMailPluginInterface::~MyKMailPluginInterface() {
}

void
MyKMailPluginInterface::createAction(KActionCollection *ac) {
    auto menu = new KActionMenu(i18n("TEST PLUGIN"), this);
    ac->addAction(QStringLiteral("autocorrect_tool"), menu);
    MessageComposer::PluginActionType type(menu, MessageComposer::PluginActionType::Tools);
    setActionType(type);
}

void
MyKMailPluginInterface::exec() {
}
