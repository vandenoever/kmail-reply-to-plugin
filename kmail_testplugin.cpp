#include "kmail_testplugin.h"
#include "kmail_testplugininterface.h"
#include <KPluginFactory>
#include <QFile>

K_PLUGIN_CLASS_WITH_JSON(MyKMailPlugin, "kmail_mykmailplugin.json")

MyKMailPlugin::MyKMailPlugin(QObject *parent, const QList<QVariant> &)
   : MessageComposer::PluginEditor(parent) {
    QFile file("/tmp/proof");
    file.open(QIODevice::WriteOnly);
    const char* msg = "i was here";
    file.write(msg, qstrlen(msg));
    file.close();
}

MyKMailPlugin::~MyKMailPlugin() {
}

MessageComposer::PluginEditorInterface *
MyKMailPlugin::createInterface(QObject *parent) {
    return new MyKMailPluginInterface(parent);
}

#include "kmail_testplugin.moc"
