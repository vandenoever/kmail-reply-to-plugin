#pragma once

#include <MessageComposer/PluginEditor>
#include <MessageComposer/PluginEditorInterface>
#include <QVariant>

class MyKMailPlugin : public MessageComposer::PluginEditor {
    Q_OBJECT
public:
    explicit MyKMailPlugin(QObject *parent = nullptr, const QList<QVariant> & = QList<QVariant>());
    ~MyKMailPlugin() override;
    MessageComposer::PluginEditorInterface *createInterface(QObject *parent = nullptr) override;
};
