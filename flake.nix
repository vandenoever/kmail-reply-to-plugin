{
  description = "KMail plugin";
  inputs = {
  };
  outputs = {self, nixpkgs }: {
    defaultPackage.x86_64-linux =
      with import nixpkgs { system = "x86_64-linux"; };
    let mkDerivation =    
        pkgs.libsForQt5.callPackage ({ mkDerivation }: mkDerivation) {};
    in 
      mkDerivation {
        name = "mykmailplugin";
        src = self;
        nativeBuildInputs = [ extra-cmake-modules ];
        buildInputs = [
          qt5.qttools
          libsForQt5.kcoreaddons
          libsForQt5.ki18n
          libsForQt5.kxmlgui
          kdeApplications.messagelib
          kdeApplications.libkdepim
          kdeApplications.pimcommon
        ];
#        configurePhase = ''
#          echo OUT: $out/$qtPluginPrefix
#          echo SRC: $src
#          #cmake -GNinja -DKDE_INSTALL_QTPLUGINDIR=$out/$qtPluginPrefix -DKDE_INSTALL_PLUGINDIR=$out/$qtPluginPrefix .
#          cmake -GNinja -DKDE_INSTALL_PLUGINDIR=$out/$qtPluginPrefix .
#        '';
      };
  };
}
